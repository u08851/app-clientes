import { Injectable } from '@angular/core';
import { clientesList } from './clienetes.json'; 
import { Cliente } from './cliente';

import { HttpClient, HttpHeaders } from "@angular/common/http";
// catchError:  --> para control de errores
import { map, catchError } from "rxjs/operators";

//.... investigar ---> Observable y of
import { of, Observable, throwError } from "rxjs";

import swal from 'sweetalert2';

import { Router } from '@angular/router';


@Injectable()

// @Injectable({
//   providedIn: 'root'
// })

export class ClienteService {
  
  private url: string ="http://localhost:8080/api/clientes" // api desde el backend contienede el crud

  private httpHeaders = new HttpHeaders({'Content-Type':'application/json'});
  
  constructor(private httpVariable: HttpClient, private router : Router) { } //inyectamos


  //--------get cliente-------------
  getClientes():Observable<Cliente[]> {
    // return of(clientesList);
    //1.
    return this.httpVariable.get<Cliente[]>(this.url);
    //2.
    // return this.httpVariable.get(tgihis.url).pipe(
    //     map(response => response as Cliente[]) 
    // );
  }

  //--------registrar clientes ------------
  getCliente(id): Observable<Cliente> {
    return this.httpVariable.get<Cliente>(`${this.url}/${id}`).pipe(
        catchError(e => {
          this.router.navigate(['/clientes']);
          console.error(e.error.mensaje);
          swal.fire('Error al editar', e.error.mensaje, 'error');
          return throwError(e);
        })
    );
  }

  //investigar Observable
  //insertar
  create(cliente: Cliente): Observable<Cliente> {
     return this.httpVariable.post(this.url, cliente, {headers : this.httpHeaders}).pipe(
        map((response: any) => response.cliente as Cliente),
        catchError(e =>{
            console.error(e.error.mensaje);
            swal.fire(e.error.mensaje, e.error.error, 'error');
            return throwError(e);
        })
     );
  }

  //actualizar
  updateCliente(cliente: Cliente): Observable<any> { // any: devuelve cualquier tipo de dato
      return this.httpVariable.put<any>(`${this.url}/${cliente.id}`, cliente, {headers : this.httpHeaders}).pipe(
        catchError(e => {
          this.router.navigate(['/clientes']);
          console.error(e.error.mensaje);
          swal.fire(e.error.mensaje, e.error.error, 'error');
          return throwError(e);
        })
    );
  }


  //eliminar
  deleteCliente(id: number): Observable<Cliente>{
    return this.httpVariable.delete<Cliente>(`${this.url}/${id}`, {headers : this.httpHeaders}).pipe(
      catchError(e => {
        this.router.navigate(['/clientes']);
        console.error(e.error.mensaje);
        swal.fire(e.error.mensaje, e.error.error, 'error');
        return throwError(e);
      })
  );
  }
  

  

}


/**
 * Nota: el service es quien se comunica con el backed
 */
