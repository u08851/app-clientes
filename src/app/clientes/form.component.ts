import { Component, OnInit } from '@angular/core';
import { Cliente } from './cliente';
import { ClienteService } from './cliente.service';

import { Router, ActivatedRoute } from '@angular/router';
import swal  from "sweetalert2";

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html'
})
export class FormComponent implements OnInit {

  private cliente: Cliente = new Cliente();
  private titulo: string = "Crear Cliente";
  // activateRoute: any;
  
  //inyectar
  constructor(
    private clienteService: ClienteService,
    private router: Router,
    private activateRoute: ActivatedRoute) { }

  ngOnInit() {
     this.cargarCliente();  //cuando se inicializa el componente
  }

  //cargarCliente: cargar cliente por id
  cargarCliente(): void {
    //class ActivatedRoute: capturamos el  id de la url 
    this.activateRoute.params.subscribe(params => { 
          let id = params['id']
          if(id){
            this.clienteService.getCliente(id).subscribe((cliente) => {
              console.log(cliente);
              this.cliente = cliente
            });
          }
      });
  }

  //create: regitrar cliente
  public create(): void{
    this.clienteService.create(this.cliente)
    .subscribe(cliente => {
      console.log(cliente);
      // response => this.router.navigate(['/clientes']) // redireccionamos a la  lista  
        this.router.navigate(['/clientes']) // redireccionamos a la  lista
        swal.fire('Nuevo Cliente', `El cliente ${cliente.nombre} a sido creado con éxito`, 'success')
      } 
    );
  }

  updateCliente(): void {
    this.clienteService.updateCliente(this.cliente)
    .subscribe( json => {
      this.router.navigate(['/clientes'])
      swal.fire('Cliente Actualizado', `${json.mensaje}: ${json.cliente.nombre}`, 'success')
    }

    );
  }

}
