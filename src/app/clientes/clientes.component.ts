import { Component, OnInit } from '@angular/core';
import { Cliente } from './cliente';
import { ClienteService } from './cliente.service';

import swal from 'sweetalert2';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html'
})

export class ClientesComponent implements OnInit {
  clientesArray: Cliente[];



  // constructor(clienteService: ClienteService) { 
  //   this.ServiceCliente = clienteService;
  // }
  constructor(private serviceCliente: ClienteService) { }

  ngOnInit() { //evento cuando se inicia el componente
    this.serviceCliente.getClientes().subscribe(

      //1.
      (clientesArray) => this.clientesArray = clientesArray

      //2.
      // (clientesArray) => {
      //   this.clientesArray = clientesArray
      // }

      //3.
      // function (clientesArray){
      //   this.clientesArray = clientesArray
      // }

    );
  }

  deleteCliente(cliente: Cliente): void {

    swal.fire({
      title: 'Estas Seguro',
      text: `¡Seguro que desea eliminar cliente? ${cliente.nombre} ${cliente.apellido}`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si Eliminar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {
        
          this.serviceCliente.deleteCliente(cliente.id).subscribe(
            response => {
              this.clientesArray = this.clientesArray.filter(cli => cli!==cliente)
              swal.fire(
              'Cliente Eliminado',
              `Cliente ${cliente.nombre} eliminad con exito`,
              'success'
              )
            }
        )
      }
    })



  }

}
